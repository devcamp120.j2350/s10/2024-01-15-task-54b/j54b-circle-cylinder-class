package model;

public class Cylinder extends Circle {
    private double height = 1;

    public Cylinder() {
        super();
    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, String color) {
        super(radius, color);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public Cylinder(double radius, String color, double height) {
        super(radius, color);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getVolumn() {
        return this.getArea() * this.height;
    }

    @Override
    public String toString() {
        return "Cylinder [height=" + height + ", radius = " + this.getRadius() + ", color = " + this.getColor() + ", area = " + this.getArea() + ", volumn =" + this.getVolumn() + "]";
    }
    
}
