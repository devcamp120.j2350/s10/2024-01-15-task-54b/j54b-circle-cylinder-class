import model.Circle;
import model.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Circle circle1 = new Circle();
        System.out.println(circle1);

        Circle circle2 = new Circle(2);
        System.out.println(circle2);

        Circle circle3 = new Circle(3, "green");
        System.out.println(circle3);

        Cylinder cylinder1 = new Cylinder();
        System.out.println(cylinder1);

        Cylinder cylinder2 = new Cylinder(2.5);
        System.out.println(cylinder2);

        Cylinder cylinder3 = new Cylinder(3.5, 1.5);
        System.out.println(cylinder3);

        Cylinder cylinder4 = new Cylinder(3.5, "green", 1.5);
        System.out.println(cylinder4);
    }
}
